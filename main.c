// #include "phoneBook.h"
#include <stdio.h>
#include <string.h>

typedef struct {
  char name[50];
  int phoneNumber;
} Person;

void insertAtPosition(Person phoneBook[], int *noOfContacts, int position,
                      char name[50], int phoneNumber);

void traversePhoneBook(Person phoneBook[], int noOfContacts);

void deleteAtPosition(Person phoneBook[], int *noOfContacts, int position);

void deleteDuplicates(Person phoneBook[], int *noOfContacts);

void searchContact(Person phoneBook[], int noOfContacts,
                   int numberToSearch); // Use Binary Search

void sortByName(Person phoneBook[], int noOfContacts); // Use Bubble Sort

void sortByNumber(Person phoneBook[],
                  int noOfContacts); // Use Insertion/Quick Sort
int binarySearch(Person phoneBook[], int left, int right, int numberToSearch);

int main() {
  int appRunning = 1;

  Person phoneBook[100] = {
      {"Debanjan Barman", 123423432},
      {"Dinabandhu Barman", 234534535},
  };
  int noOfContacts = 2;

  while (appRunning) {
    printf("\nPlease select one Option\n1: To Display all the contacts\n2: To "
           "insert a contact at given position\n3: To "
           "Delete a Contact at given position\n4: To Delete Duplicate "
           "Contacts\n5: Search a Contact by Number\n6: Sort all the Contacts "
           "By their Name\n7: Sort by phone number\n0: To Exit\n>>> ");

    int choice;
    scanf("%d", &choice);

    switch (choice) {
    case 0: {
      appRunning = 0;
      break;
    }
    case 1: {
      traversePhoneBook(phoneBook, noOfContacts);
      break;
    }
    case 2: {
      int position = 0;
      char name[50] = {};
      int number = {};

      printf("Please enter position to insert\n>> ");
      scanf("%d", &position);

      if (position < 1 || position > noOfContacts + 1) {
        printf("\nPlease enter a position between [1, %d]\n", noOfContacts + 1);
        break;
      }

      printf("Please chose the name(Without any space)\n>> ");
      scanf("%s", name);
      printf("Please enter the number\n>> ");
      scanf("%d", &number);

      insertAtPosition(phoneBook, &noOfContacts, position, name, number);
      traversePhoneBook(phoneBook, noOfContacts);
      break;
    }
    case 3: {
      if (!noOfContacts) {
        printf("\nThe PhoneBook Is Empty\n");
        break;
      }

      int position = 0;
      printf("Please enter position to delete\n>> ");
      scanf("%d", &position);

      if (position > noOfContacts) {
        printf("\nPlease enter a position between [1, %d]\n", noOfContacts + 1);
        break;
      }

      deleteAtPosition(phoneBook, &noOfContacts, position);
      traversePhoneBook(phoneBook, noOfContacts);
      break;
    }
    case 4: {
      deleteDuplicates(phoneBook, &noOfContacts);
      break;
    }
    case 5: {
      int numberToSearch;
      printf("\nPlease enter a number to Search: ");
      scanf("%d", &numberToSearch);
      searchContact(phoneBook, noOfContacts, numberToSearch);
      break;
    }
    case 6: {
      sortByName(phoneBook, noOfContacts);
      printf("\n------- SORTED BY NAME ---------\n");
      break;
    }
    case 7: {
      sortByNumber(phoneBook, noOfContacts);
      printf("\n------- SORTED BY PHONE NUMBER ---------\n");
      break;
    }
    default:
      printf("\nWrong Choice\n");
    }
  }

  return 0;
}

void insertAtPosition(Person phoneBook[], int *noOfContacts, int position,
                      char name[], int phoneNumber) {
  int index = position - 1;
  int i;

  Person newPerson = {};
  newPerson.phoneNumber = phoneNumber;
  strcpy(newPerson.name, name);

  for (i = *noOfContacts - 1; i > index; --i) {
    phoneBook[i + 1] = phoneBook[i];
  }
  phoneBook[index] = newPerson;
  *noOfContacts += 1;
}

void traversePhoneBook(Person phoneBook[], int noOfContacts) {
  int i;

  if (!noOfContacts) {
    printf("\nThe Phone Book is Empty\n");
  }

  for (i = 0; i < noOfContacts; ++i) {
    printf("\n----------------\nContact no %d\n---------------\n", i + 1);
    printf("Name: %s\nPhone Number: %d\n", phoneBook[i].name,
           phoneBook[i].phoneNumber);
  }
}

void deleteAtPosition(Person phoneBook[], int *noOfContacts, int position) {
  int index = position - 1;
  int i;
  for (i = index; i < *noOfContacts; ++i) {
    phoneBook[i] = phoneBook[i + 1];
  }
  *noOfContacts -= 1;
}

void deleteDuplicates(Person phoneBook[], int *noOfContacts) {
  int i, j;
  for (i = 0; i < *noOfContacts; ++i) {
    for (j = i + 1; j < *noOfContacts;) {
      if (phoneBook[i].phoneNumber == phoneBook[j].phoneNumber) {
        deleteAtPosition(phoneBook, noOfContacts, j + 1);
      } else {
        j++;
      }
    }
  }
}

void searchContact(Person phoneBook[], int noOfContacts, int numberToSearch) {
  // Use Binary Search
  int i;
  sortByNumber(phoneBook, noOfContacts);

  i = binarySearch(phoneBook, 0, noOfContacts, numberToSearch);

  printf("\n----------------\nContact no %d\n---------------\n", i + 1);
  printf("Name: %s\nPhone Number: %d\nPosition: %d\n", phoneBook[i].name,
         phoneBook[i].phoneNumber, i + 1);
}

void sortByName(Person phoneBook[], int noOfContacts) {
  // Use Bubble Sort

  int i, j;
  Person tempPerson;

  for (i = 0; i < noOfContacts - 1; ++i) {
    for (j = 0; j < noOfContacts - i - 1; ++j) {
      if (strcmp(phoneBook[j].name, phoneBook[j + 1].name) > 0) {
        // Swap the two persons
        tempPerson = phoneBook[j];
        phoneBook[j] = phoneBook[j + 1];
        phoneBook[j + 1] = tempPerson;
      }
    }
  }
}

void sortByNumber(Person phoneBook[], int noOfContacts) {
  // Use Insertion/Quick Sort
  int i, j;
  Person currentPerson;

  for (i = 1; i < noOfContacts; ++i) {
    j = i - 1;

    currentPerson = phoneBook[i];

    while (phoneBook[j].phoneNumber > currentPerson.phoneNumber && j >= 0) {
      phoneBook[j + 1] = phoneBook[j];
      j--;
    }
    phoneBook[j + 1] = currentPerson;
  }
}

int binarySearch(Person phoneBook[], int left, int right, int numberToSearch) {
  while (left <= right) {
    int mid = left + (right - left) / 2;

    if (phoneBook[mid].phoneNumber == numberToSearch) {
      return mid;
    }

    if (phoneBook[mid].phoneNumber < numberToSearch) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
  }
  return -1;
}