#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
 char music[50];
 struct Node *prev;
 struct Node *next;
}Node;

void addMusic(Node **HEAD,Node **current){
char musicName[50];
 Node *newNode = (Node*) malloc(sizeof(Node));
 printf("Please enter the music name: ");
 scanf("%s",musicName);
 strcpy(newNode->music,musicName);

 if(*HEAD ==NULL){
  *HEAD = newNode;
  newNode->prev = *HEAD;
  newNode->next = *HEAD;
  *current = *HEAD;
 }else{
  newNode->next = *HEAD;
  newNode->prev = (*HEAD)->prev;
  (*HEAD)->prev->next = newNode;
  (*HEAD)->prev =newNode;
 }
 return;
}
void playMusic(Node **current){
 if(*current ==NULL){
  printf("Playlist is empty\n");
  return;
 }
 printf("Playing music: %s\n",(*current)->music);
}
void prevMusic(Node **current){
 *current = (*current)->prev;
 playMusic(current);
}
void nextMusic(Node **current){
 *current = (*current)->next;
 playMusic(current);
}

int main(){
int running=1;
int choice;
Node *HEAD=NULL,*current =NULL;

clrscr();
do{
printf("\n___MENU___\n1.Play\n2.Add music\n3.Next music\n4.Prev music\n0.Exit\n>>>");
scanf("%d",&choice);

  switch(choice){
   case 0:
    running =0;
    break;

   case 1:
    //Play music
    playMusic(&current);
    break;

   case 2:
    //Add music
    addMusic(&HEAD,&current);
    break;
   case 3:
    //next music
    nextMusic(&current);
    break;
   case 4:
    //prev music
    prevMusic(&current);
    break;
   default:
    printf("wrong choice\n");
    break;
  }


}while(running==1);

 return 0;
}

