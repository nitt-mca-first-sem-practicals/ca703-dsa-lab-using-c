#include <stdio.h>
#include <stdlib.h>

struct Node {
  int data;
  struct Node *next;
};

typedef struct Node Node;

Node *insert_in_middle(Node *head, int value_to_insert, int position) {
  Node *new_node = (Node *)malloc(sizeof(Node));
  new_node->data = value_to_insert;
  new_node->next = NULL;

  if (position == 1) {
    new_node->next = head;
    head = new_node;
    return head;
  }

  Node *current = head;
  for (int i = 1; i < position - 1 && current != NULL; i++) {
    current = current->next;
  }

  if (current == NULL) {
    printf("Position out of range\n");
    return head;
  }

  new_node->next = current->next;
  current->next = new_node;

  return head;
}

Node *remove_elements(Node *head, int value_to_remove) {
  Node *dummy = (Node *)malloc(sizeof(Node));
  dummy->next = head;
  Node *current = dummy;

  while (current->next != NULL) {
    if (current->next->data == value_to_remove) {
      Node *temp = current->next;
      current->next = temp->next;
      free(temp);
    } else {
      current = current->next;
    }
  }

  Node *new_head = dummy->next;
  free(dummy);
  return new_head;
}

void print_list(Node *head) {
  Node *current = head;
  while (current != NULL) {
    printf("%d -> ", current->data);
    current = current->next;
  }
  printf("NULL\n");
}

int main() {
  Node *head = NULL;

  // Create a linked list with 5 numbers: 1, 2, 3, 4, 5
  for (int num = 5; num >= 1; num--) {
    Node *new_node = (Node *)malloc(sizeof(Node));
    new_node->data = num;
    new_node->next = head;
    head = new_node;
  }

  // Printing the original linked list
  printf("Original Linked List: ");
  print_list(head);

  // Insert an element with value 10 at position 3
  head = insert_in_middle(head, 10, 3);
  printf("After insert: ");
  print_list(head);

  // Removing all occurrences of value 3 from the linked list
  head = remove_elements(head, 3);
  printf("After delete: ");
  print_list(head);

  // Freeing allocated memory
  Node *current = head;
  while (current != NULL) {
    Node *temp = current;
    current = current->next;
    free(temp);
  }

  return 0;
}
