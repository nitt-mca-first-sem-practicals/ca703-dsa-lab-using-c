#include <stdio.h>
#include <stdlib.h>

struct Node {
  int data;
  struct Node *next;
};

struct Node *create_node(int data) {
  struct Node *new_node = (struct Node *)malloc(sizeof(struct Node));
  new_node->data = data;
  new_node->next = NULL;
  return new_node;
}

void insert_node(struct Node **head, int data) {
  struct Node *new_node = create_node(data);

  if (*head == NULL) {
    *head = new_node;
  } else {
    struct Node *current_node = *head;
    while (current_node->next != NULL) {
      current_node = current_node->next;
    }
    current_node->next = new_node;
  }
}

void print_list(struct Node *head) {
  struct Node *current_node = head;
  while (current_node != NULL) {
    printf("%d ", current_node->data);
    current_node = current_node->next;
  }
  printf("\n");
}

void add_element_in_middle(struct Node **head, int data, int position) {
  struct Node *new_node = create_node(data);
  struct Node *current_node = *head;
  int count = 0;

  while (count < position - 1) {
    current_node = current_node->next;
    count++;
  }

  struct Node *next_node = current_node->next;
  current_node->next = new_node;
  new_node->next = next_node;
}

void remove_all_elements(struct Node **head, int value) {
  struct Node *current_node = *head;
  struct Node *previous_node = NULL;

  while (current_node != NULL) {
    if (current_node->data == value) {
      if (previous_node != NULL) {
        previous_node->next = current_node->next;
      } else {
        *head = current_node->next;
      }
      free(current_node);
    } else {
      previous_node = current_node;
    }
    current_node = current_node->next;
  }
}

int main() {
  struct Node *head = NULL;

  insert_node(&head, 1);
  insert_node(&head, 2);
  insert_node(&head, 3);
  insert_node(&head, 4);
  insert_node(&head, 5);

  print_list(head);

  printf("Adding element and position 2\n");
  add_element_in_middle(&head, 6, 2);
  print_list(head);


  printf("Deleting all element with value 2\n");
  remove_all_elements(&head, 2);
  print_list(head);

  return 0;
}
