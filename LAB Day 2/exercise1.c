#include <stdio.h>

int linear_search(int arr[], int size, int target) {
  int i;
  for (i = 0; i < size; i++) {
    if (arr[i] == target) {
      return 1;
    }
  }
  return 0;
}

int binary_search(int arr[], int size, int target) {
  int left = 0;
  int right = size - 1;

  while (left <= right) {
    int mid = left + (right - left) / 2;

    if (arr[mid] == target) {
      return 1; // Element found
    }

    if (arr[mid] < target) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
  }

  return 0; // Element not found
}

int main() {
  int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  int size = sizeof(arr) / sizeof(arr[0]);
  int element;
  int choice;
  printf("Please enter an element  to search: ");
  scanf("%d", &element);

  printf("Please select an algorithm to search\n1. Linear search\n2. Binary "
         "Search\n>>");
  scanf("%d", &choice);

  switch (choice) {
  case 1:
    if (linear_search(arr, size, element)) {
      printf("Element found using Linear Search.\n");
    } else {
      printf("Element not found.\n");
    }
    break;
  case 2:
    if (binary_search(arr, size, element)) {
      printf("Element found using Binary Search.\n");
    } else {
      printf("Element not found.\n");
    }
    break;
  default:
    printf("Wrong choice\n");
    return 0;
  }

  return 0;
}