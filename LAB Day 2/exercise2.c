#include <stdio.h>

// Define the Student structure
typedef struct {
  int rollNumber;
  char name[50];
  char grade;
} Student;

// Function to display students
void displayStudents(Student arr[], int n) {
  for (int i = 0; i < n; i++) {
    printf("Roll Number: %d, Name: %s, Grade: %c\n", arr[i].rollNumber,
           arr[i].name, arr[i].grade);
  }
  printf("\n");
}

// Bubble sort by grade
void bubbleSort(Student arr[], int n) {
  for (int i = 0; i < n - 1; i++) {
    for (int j = 0; j < n - i - 1; j++) {
      if (arr[j].grade > arr[j + 1].grade) {
        // Swap students
        Student temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
}

// Quicksort by roll number
int partition(Student arr[], int low, int high) {
  int pivot = arr[low].rollNumber;
  int left = low + 1;
  int right = high;
  while (1) {
    while (left <= right && arr[left].rollNumber <= pivot)
      left++;
    while (arr[right].rollNumber >= pivot && right >= left)
      right--;
    if (right < left)
      break;
    // Swap students
    Student temp = arr[left];
    arr[left] = arr[right];
    arr[right] = temp;
  }
  // Swap students
  Student temp = arr[low];
  arr[low] = arr[right];
  arr[right] = temp;
  return right;
}

void quickSort(Student arr[], int low, int high) {
  if (low < high) {
    int pivotIndex = partition(arr, low, high);
    quickSort(arr, low, pivotIndex);
    quickSort(arr, pivotIndex + 1, high);
  }
}

int main() {
  // Create an array of Student objects
  Student students[] = {
      {101, "Alice", 'A'}, {104, "David", 'A'}, {103, "Charlie", 'C'},
      {105, "Eve", 'D'},   {102, "Bob", 'B'},
  };
  int numStudents = sizeof(students) / sizeof(students[0]);

  // Display elements before sorting
  printf("Before sorting:\n");
  displayStudents(students, numStudents);

  // Sort by grade using bubble sort
  bubbleSort(students, numStudents);
  printf("Sorted by Grade:\n");
  displayStudents(students, numStudents);

  // Sort by roll number using quicksort
  quickSort(students, 0, numStudents - 1);
  printf("Sorted by Roll Number:\n");
  displayStudents(students, numStudents);

  return 0;
}
