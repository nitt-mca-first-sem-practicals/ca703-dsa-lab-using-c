#include <stdio.h>

#define MAX_SIZE 100

// Function to perform linear search
int linearSearch(int arr[], int size, int target) {
  int i;
  for (i = 0; i < size; i++) {
    if (arr[i] == target) {
      return i; // Return the position (0-based) of the element found
    }
  }
  return -1; // Element not found
}

// Function to perform bubble sort
void bubbleSort(int arr[], int size) {
  int i, j;

  for (i = 0; i < size - 1; i++) {
    for (j = 0; j < size - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        // Swap elements
        int temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
}

// Function to perform binary search (uses bubble sort internally)
int binarySearch(int arr[], int size, int target) {
  int low = 0;
  int high = size - 1;
  int mid;
  bubbleSort(arr, size);

  while (low <= high) {
    mid = (low + high) / 2;
    if (arr[mid] == target) {
      return mid; // Return the position (0-based) of the element found
    } else if (arr[mid] < target) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }
  return -1; // Element not found
}

int main() {
  int choice, size = 0, element, result;
  int i;
  int arr[MAX_SIZE];

  do {
    printf("\nMenu:\n");
    printf("1. Insert new element\n");
    printf("2. Display elements\n");
    printf("3. Linear Search\n");
    printf("4. Binary Search\n");
    printf("5. Exit\n");
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch (choice) {
    case 1:
      if (size < MAX_SIZE) {
        printf("Enter the element to insert: ");
        scanf("%d", &arr[size]);
        size++;
      } else {
        printf("Array is full.\n");
      }
      break;

    case 2:
      if (size > 0) {
        printf("Elements in the array: \n");
        for (i = 0; i < size; i++) {
          printf("%d ", arr[i]);
        }
        printf("\n");
      } else {
        printf("Array is empty.\n");
      }
      break;

    case 3:
      if (size > 0) {
        printf("Enter the element to search: ");
        scanf("%d", &element);
        result = linearSearch(arr, size, element);
        if (result != -1) {
          printf("%d found at position %d in the array.\n", element, result);
        } else {
          printf("%d not found in the array.\n", element);
        }
      } else {
        printf("Array is empty.\n");
      }
      break;

    case 4:
      if (size > 0) {
        printf("Enter the element to search: ");
        scanf("%d", &element);
        result = binarySearch(arr, size, element);
        if (result != -1) {
          printf("%d found at position %d in the array.\n", element, result);
        } else {
          printf("%d not found in the array.\n", element);
        }
      } else {
        printf("Array is empty.\n");
      }
      break;

    case 5:
      printf("Exiting the program.\n");
      break;

    default:
      printf("Invalid choice. Please select a valid option.\n");
      break;
    }
  } while (choice != 5);

  return 0;
}
