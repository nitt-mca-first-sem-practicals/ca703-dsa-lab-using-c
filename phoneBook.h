#ifndef PHONEBOOK_H
#define PHONEBOOK_H

typedef struct {
  char name[50];
  int phoneNumber;
} Person;

void insertAtPosition(Person phoneBook[], int *noOfContacts, int position,
                      char name[50], int phoneNumber);

void traversePhoneBook(Person phoneBook[], int noOfContacts);

void deleteAtPosition(Person phoneBook[], int *noOfContacts, int position);

void deleteDuplicates(Person phoneBook[], int *noOfContacts);

void searchContact(Person phoneBook[], int noOfContacts,
                   int numberToSearch); // Use Binary Search

void sortByName(Person phoneBook[], int noOfContacts); // Use Bubble Sort

void sortByNumber(Person phoneBook[],
                  int noOfContacts); // Use Insertion/Quick Sort
int binarySearch(Person phoneBook[], int left, int right, int numberToSearch);
#endif
